package Math::NLopt::Exception;

# ABSTRACT: Basic Exception Classes

use v5.12;
use strict;
use warnings;

#<<<

our $VERSION = '0.10';

#>>>


use overload q{""} => \&message;

=class_method new

  $object = Math::NLopt::Exception->new( $message );

Construct an object containing the following method

=cut

sub new {
    my $class   = shift;
    my $message = shift;
    return bless \$message, $class;
}

=method message

  $message = $object->message

retrieve an object's message

=cut


sub message {
    my $self = shift;
    return $$self;
}



{
    package Math::NLopt::Exception::Failure;
    use parent -norequire => 'Math::NLopt::Exception';
}

{
    package Math::NLopt::Exception::OutOfMemory;
    use parent -norequire => 'Math::NLopt::Exception';
}

{
    package Math::NLopt::Exception::InvalidArgs;
    use parent -norequire => 'Math::NLopt::Exception';
}

{
    package Math::NLopt::Exception::RoundoffLimited;
    use parent -norequire => 'Math::NLopt::Exception';
}

{
    package Math::NLopt::Exception::ForcedStop;
    use parent -norequire => 'Math::NLopt::Exception';
}


1;

# COPYRIGHT

__END__

=head1 SYNOPSIS

  use Math::NLopt::Exception;

  croak( Math::NLopt::Exception::Failure->new( "error messsage" ) );

=head1 DESCRIPTION

This is a very simple exception class used by L<Math::NLopt>. Importing
this module also imports the

  Math::NLopt::Exception::Failure
  Math::NLopt::Exception::OutOfMemory
  Math::NLopt::Exception::InvalidArgs
  Math::NLopt::Exception::RoundoffLimited>
  Math::NLopt::Exception::ForcedStop

subclasses.

=head1 METHODS

=head1 OVERLOADS

The exception object overloads the stringify operation using the
L</message> method.

