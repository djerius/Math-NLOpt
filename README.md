# NAME

Math::NLopt - Math::NLopt - Perl interface to the NLopt optimization library

# VERSION

version 0.10

# SYNOPSIS

    use Math::NLopt ':algorithms';

    my $opt = Math::NLopt->new( NLOPT_LD_MMA, 2 );
    $opt->set_lower_bounds( [ -HUGE_VAL(), 0 ] );
    $opt->set_min_objective( sub ( $x, $grad, $data ) { ... } );
    $opt->set_xtol_rel( ... );
    \@optimized_pars = $opt->optimize( \@initial_pars );

# DESCRIPTION

[NLopt](https://github.com/stevengj/nlopt) is a

    library for nonlinear local and global optimization, for functions
    with and without gradient information. It is designed as a simple,
    unified interface and packaging of several free/open-source
    nonlinear optimization libraries.

**Math::NLopt** is a Perl binding to **NLopt**.  It uses the
[Alien::NLopt](https://metacpan.org/pod/Alien%3A%3ANLopt) module to find or install a Perl local instance of the
**NLopt** library.

This module provides an interface using native Perl arrays.

The main documentation for **NLopt** may be found at [https://nlopt.readthedocs.io/](https://nlopt.readthedocs.io/); this document focuses on the
Perl specific implementation, which is more Perlish than the C API
(and is very similar to the Python one).

## API

The Perl API uses an object, constructed by the ["new"](#new) class method,
to maintain state. The optimization process is controlled by
invoking methods on the object.

_In general_ results are returned directly from the methods; method
parameters are used primarily as input data for the methods (the
objective and constraint callbacks more closely follow the C API).

The Perl methods are named similarly to the C functions, e.g.

    nlopt_<method>( opt, ... );

becomes

    $opt->method( ... );

Where `$opt` is provided by the ["new"](#new) class method.

As an example,  the C API for starting
the optimization process is

    nlopt_result nlopt_optimize(nlopt_opt opt, double *x, double *opt_f);

where **x** is used for both passing in the initial model parameters as
well as retrieving their final values. The final value of the
optimization function is stored in **opt\_f**. A code specifying the
success or failure of the process is returned.

The Perl interface (similar to the Python and C++ versions) is

    \@final = $opt->optimize( \@initial_pars );
    $opt_f = $opt->last_optimum_value;
    $result_code = $opt->last_optimize_result;

The Perl API throws exceptions on failures, similar to the behavior of
the C++ and Python API's.  Where the C API returns error codes,
`Math::NLopt` throws objects in similarly named exception classes:

    Math::NLopt::Exception::Failure
    Math::NLopt::Exception::OutOfMemory
    Math::NLopt::Exception::InvalidArgs
    Math::NLopt::Exception::RoundoffLimited>
    Math::NLopt::Exception::ForcedStop

These all extend the [Math::NLopt::Exception](https://metacpan.org/pod/Math%3A%3ANLopt%3A%3AException) class; see it for more
information on retrieving messages from the objects.

## Constants

**Math::NLopt** defines constants for the optimization algorithms,
result codes, and utilities.

The algorithm constants have the same names as the **NLopt** constants,
and may be imported individually by name or en-masse with the
':algorithms' tag:

    use Math::NLopt 'NLOPT_LD_MMA';
    use Math::NLopt ':algorithms';

Importing result codes is similar:

    use Math::NLopt 'NLOPT_FORCED_STOP';
    use Math::NLopt ':results';

As are the utility subroutines:

    use Math::NLopt 'algorithm_from_string';
    use Math::NLopt ':utils';

## Callbacks

**NLopt** handles the optimization of the objective function, relying
upon user provided subroutines to calculate the objective function and
non-linear constraints (see below for the required calling signature).

The callback subroutines are called with a user-provided structure
which can be used to pass additional information to the callback
(or the subroutines can use closures).

### Objective Functions

Objective functions callbacks are registered via either

    $opt->set_min_objective( \&func, ?$data );
    $opt->set_max_objective( \&func, ?$data );

where `$data` is an optional structure passed to the callback which
can be used for any purpose.

The objective function has the signature

    $value = sub ( \@params, \@gradient, $data ) { ... }

It returns the value of the optimization function for the
passed set parameters, **@params**.

if **\\@gradient** is not `undef`, it must be filled in by the
objective function.

`$data` is the structure registered with the callback. It will be
`undef` if none was provided.

## Non-linear Constraints

Nonlinear constraint callbacks are registered via either of

    $opt->add_equality_constraint( \&func, ?$data, ?$tol = 0 );
    $opt->add_inequality_constraint( \&func, ?$data, ?$tol = 0 );

where `$data` is an optional structure passed to the callback which
can be used for any purpose, and `$tol` is a tolerance.  Pass
`undef` for `$data` if a tolerance is required but `$data` is not.

The callbacks have the same signature as the objective callbacks.

### Vector-valued Constraints

Vector-valued constraint callbacks are registered via either of

    $opt->add_equality_mconstraint( \&func, $m, ?$data, ?\@tol );
    $opt->add_inequality_mconstraint( \&func, $m, ?$data, ?\@tol );

where `$m` is the length of the vector, `$data` is an optional
structure passed on to the callback function, and `@tol` is an
optional array of length `$m` containing the tolerance for each
component of the vector

Vector valued constraints callbacks have the signature

    sub ( \@result, \@params, \@gradient, $data ) { ... }

The `$m` length vector of constraints should be stored in `\@result`.
If `\@gradient` is not `undef`, it is a _$n x $m_ length
array which should be filled by the callback.

`$data` is the optional structure passed to the callback.

### Preconditioned Objectives

These are registered via one of

    $opt->set_precond_min_objective( \&func, \&precond, ?$data);
    $opt->set_precond_max_objective( \&func, \&precond, ?$data);

`\&func` has the same signature as before (see ["Objective Functions"](#objective-functions)),
and `$data` is as before.

The `\&precond` fallback has this signature:

    sub (\@x, \@v, \@vpre, $data) {...}

`\@x`, `\@v`, and `\@vpre` are arrays of length `$n`.
`\@x`, `\@v`  are input and `\@vpre` should be filled in by the routine.

# METHODS

Most methods have the same calling signature as their C versions, but
not all!

### add\_equality\_constraint

    $opt->add_equality_constraint( \&func, ?$data, ?$tol = 0 );

### add\_equality\_mconstraint

    $opt->add_equality_mconstraint( \&func, $m, ?$data, ?\@tol );

### add\_inequality\_constraint

    $opt->add_inequality_constraint( \&func, ?$data, ?$tol = 0 );

### add\_inequality\_mconstraint

    $opt->add_inequality_mconstraint( \&func, $m, ?$data, ?\@tol );

### force\_stop

    $opt->force_stop;

### get\_algorithm

    $algorithm_int_id = $opt->get_algorithm;

### get\_dimension

    $n = $opt->get_dimension;

### get\_errmsg

    $string  $opt->get_errmsg;

### get\_force\_stop

    $stop = $opt->get_force_stop;

### get\_ftol\_abs

    $tol = $opt->get_ftol_abs;

### get\_ftol\_rel

    $tol = $opt->get_ftol_rel;

### get\_initial\_step

    \@steps = $opt->get_initial_step( \@init_x );

### get\_lower\_bounds

    \@lb = $opt->get_lower_bounds;

### get\_maxeval

    $max_eval = $opt->get_maxeval;

### get\_maxtime

    $max_time = $opt->get_maxtime;

### get\_numevals

    $num_evals = $opt->get_numevals;

### get\_param

    $val = $opt->get_param( $name, $defaultval);

Return parameter value, or `$defaultval` if not set.

### get\_population

    $pop = $opt->get_population;

### get\_stopval

    $val = $opt->get_stopval;

### get\_upper\_bounds

    \@ub = $opt->get_upper_bounds;

### get\_vector\_storage

    $dim = $opt->get_vector_storage;

### get\_x\_weights

    \@weights = $opt->get_x_weights;

### get\_xtol\_abs

    \@tol = $opt->get_xtol_abs;

### get\_xtol\_rel

    $tol = $opt->get_xtol_rel;

### has\_param

    $bool = $opt->has_param( $name );

True if the parameter with `$name` was set.

### nth\_param

    $name = $opt->nth_param( $i );

Return the name of algorithm specific parameter `$i`.

### last\_optimize\_result

    $result_code = $opt->last_optimize_result;

Return the result code after an optimization.

### last\_optimum\_value

    $min_f = $opt->last_optimum_value;

Return the objective value obtained after an optimization.

### num\_params

    $n_algo_params = $opt->num_params;

Return the number of algorithm specific parameters.

### optimize

    \@optimized_pars = $opt->optimize( \@input_pars );

Returns the parameter values determined from the optimization.  The
status of the optimization (e.g. NLopt's result code) can be retrieved
via the ["last\_optimize\_result"](#last_optimize_result) method. The final value of the
objective function is available via the ["last\_aptimum\_value"](#last_aptimum_value) method.

### remove\_equality\_constraints

    $opt->remove_equality_constraints;

### remove\_inequality\_constraints

    $opt->remove_inequality_constraints;

### set\_force\_stop

    $opt->set_force_stop( $val );

### set\_ftol\_abs

    $opt->set_ftol_abs( $tol );

### set\_ftol\_rel

    $opt->set_ftol_rel( $tol );

### set\_initial\_step

    $opt->set_initial_step(\@dx);

`@dx` has length `$n`.

### set\_initial\_step1

    $opt->set_initial_step1( $dx );

### set\_local\_optimizer

    $opt->set_local_optmizer( $local_opt );

### set\_lower\_bound

    $opt->set_lower_bound( $i, $ub );

Set the lower bound for parameter `$i` (zero based) to `$ub`

### set\_lower\_bounds

    $opt->set_lower_bounds(\@ub);

`@ub` has length `$n`.

### set\_lower\_bounds1

    $opt->set_lower_bounds1 ($ub);

### set\_max\_objective

    $opt->set_max_objective( \&func, ?$data );

See ["Objective Functions"](#objective-functions)

### set\_maxeval

    $opt->set_maxeval( $max_iterations );

### set\_maxtime

    $opt->set_maxtime( $time );

### set\_min\_objective

    $opt->set_min_objective( \&func, ?$data );

See ["Objective Functions"](#objective-functions)

### set\_param

    $opt->set_param( $name, $value );

### set\_population

    $opt->set_population( $pop );

### set\_precond\_max\_objective

    $opt->set_precond_max_objective( \&func, \&precond, ?$data);

See ["Preconditioned Objectives"](#preconditioned-objectives)

### set\_precond\_min\_objective

    $opt->set_precond_min_objective( \&func, \&precond, ?$data);

See ["Preconditioned Objectives"](#preconditioned-objectives)

### set\_stopval

    $opt->set_stopval( $stopval);

### set\_upper\_bound

    $opt->set_upper_bound( $i, $ub );

Set the upper bound for parameter `$i` (zero based) to `$ub`

### set\_upper\_bounds

    $opt->set_upper_bounds(\@ub);

`@ub` has length `$n`.

### set\_upper\_bounds1

    $opt->set_upper_bounds1 ($ub);

### set\_vector\_storage

    $opt->set_vector_storage( $dim )

### set\_x\_weights

    $opt->set_x_weights( \@weights );

`@weights` has length `$n`.

### set\_x\_weights1

    $opt->set_x_weights1( $weight );

### set\_xtol\_abs

    $opt->set_xtol_abs( \@tol );

`@tol` has length `$n`.

### set\_xtol\_abs1

    $opt->set_xtol_abs1( $tol );

### set\_xtol\_rel

    $opt->set_xtol_rel( $tol );

## new

    my $opt = Math::NLopt->new( $algorithm, $n );

Create an optimization object for the given algorithm and number of parameters.
**$algorithm** is one of the algorithm constants, e.g.

    use Math::NLopt 'NLOPT_LD_MMA';
    my $opt = Math::NLopt->new( NLOPT_LD_MMA, 3 );

# CONSTRUCTORS

# UTILITY SUBROUTINES

These are exportable individually, or en-masse via the `:utils` tag,
but beware that **srand** has same name as the Perl `srand` routine, and
`version` is rather generic.

### algorithm\_from\_string

    $algorithm_int_id = algorithm_from_string( $algorithm_string_id );

return an integer id (e.g. **NLOPT\_LD\_MMA**) from a string id (e.g. 'LD\_MMA').

### algorithm\_name

    $algorithm_name = algorithm_from_string( $algorithm_int_id );

return a descriptive name from an integer id

### algorithm\_to\_string

    $algorithm_string_id = algorithm_to_string( $algorithm_int_id );

### result\_from\_string

    $result_int_id = result_from_string( $result_string_id );

return an integer id (e.g. **NLOPT\_SUCCESS**) from a string id (e.g. 'SUCCESS').

### result\_to\_string

    $result_string_id = result_to_string( $result_int_id );

### srand

    srand( $seed )

### srand\_time

### version

    ($major, $minor, $bugfix ) = Math::NLopt::version()

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-math-nlopt@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=Math-NLopt](https://rt.cpan.org/Public/Dist/Display.html?Name=Math-NLopt)

## Source

Source is available at

    https://gitlab.com/djerius/math-nlopt

and may be cloned from

    https://gitlab.com/djerius/math-nlopt.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [https://github.com/stevengj/nlopt](https://github.com/stevengj/nlopt)
- [Alien::NLopt](https://metacpan.org/pod/Alien%3A%3ANLopt)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2024 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
